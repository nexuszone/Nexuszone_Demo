/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: ['class'],
  content: [
    './pages/**/*.{ts,tsx}',
    './components/**/*.{ts,tsx}',
    './app/**/*.{ts,tsx}',
    './src/**/*.{ts,tsx}'
  ],
  prefix: '',
  theme: {
    fontFamily: {
      primary: ['Pretendard', 'serif']
    },
    container: {
      center: true,
      padding: '1.5rem',
      screens: {
        '2xl': '1440px'
      }
    },
    borderWidth: {
      DEFAULT: '1px',
      0: '0',
      2: '2px',
      3: '3px',
      4: '4px',
      6: '6px',
      8: '8px'
    },
    extend: {
      borderWidth: ['hover', 'focus'],
      colors: {
        border: 'hsl(var(--border))',
        input: 'hsl(var(--input))',
        ring: 'hsl(var(--ring))',
        background: 'hsl(var(--background))',
        foreground: 'hsl(var(--foreground))',
        primary: {
          DEFAULT: 'hsl(var(--primary))',
          foreground: 'hsl(var(--primary-foreground))'
        },
        secondary: {
          DEFAULT: 'hsl(var(--secondary))',
          foreground: 'hsl(var(--secondary-foreground))'
        },
        destructive: {
          DEFAULT: 'hsl(var(--destructive))',
          foreground: 'hsl(var(--destructive-foreground))'
        },
        muted: {
          DEFAULT: 'hsl(var(--muted))',
          foreground: 'hsl(var(--muted-foreground))'
        },
        accent: {
          DEFAULT: 'hsl(var(--accent))',
          foreground: 'hsl(var(--accent-foreground))'
        },
        popover: {
          DEFAULT: 'hsl(var(--popover))',
          foreground: 'hsl(var(--popover-foreground))'
        },
        card: {
          DEFAULT: 'hsl(var(--card))',
          foreground: 'hsl(var(--card-foreground))'
        },
        'black-1': '#0c0c18',
        'black-2': '#18182c',
        'gray-1': '#8a919f',
        'gray-2': '#f5f7fa',
        'purple-dark': 'rgb(26 24 43)',
        'purple-1': '#22223c',
        'purple-2': '#2c2c42'
      },
      backgroundSize: {
        '100%': '100%'
      },
      borderRadius: {
        lg: 'var(--radius)',
        md: 'calc(var(--radius) - 2px)',
        sm: 'calc(var(--radius) - 4px)'
      },
      keyframes: {
        'accordion-down': {
          from: { height: '0' },
          to: { height: 'var(--radix-accordion-content-height)' }
        },
        'accordion-up': {
          from: { height: 'var(--radix-accordion-content-height)' },
          to: { height: '0' }
        },
        supportanimation: {
          from: {
            transform: 'translate(0, -50%)',
            opacity: 0
          },
          to: {
            transform: 'translate(0%, 0)',
            opacity: 1
          }
        }
      },
      animation: {
        'accordion-down': 'accordion-down 0.2s ease-out',
        'accordion-up': 'accordion-up 0.2s ease-out',
        supportanimation: 'supportanimation 0.8s ease-in-out'
      },
      shadow: {},
      vertical: {
        'vertical-middle': ''
      },
      gradientColorStops: {
        custom1: 'linear-gradient(180deg, #DBFEF1, #fff 25%)'
      }
    }
  },
  plugins: [require('tailwindcss-animate')]
}
