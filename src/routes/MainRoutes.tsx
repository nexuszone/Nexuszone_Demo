import HomePage from '@/pages/HomePage'
import Layout from '../layout'
import ProductPage from '@/pages/ProductPage'
import CompanyPage from '@/pages/CompanyPage'
import NewsPage from '@/pages/NewsPage'
import ContactPage from '@/pages/ContactPage'
import NotFound from '@/components/errorPage/notFound'

const MainRoutes = {
  path: '/',
  element: <Layout />,
  children: [
    {
      path: '/',
      element: <HomePage />
    },
    {
      path: '/product',
      element: <ProductPage />
    },
    {
      path: '/company',
      element: <CompanyPage />
    },
    {
      path: '/news',
      element: <NewsPage />
    },
    {
      path: '/contact',
      element: <ContactPage />
    },
    {
      path: '*',
      element: <NotFound />
    }
  ]
}

export default MainRoutes
