import { Outlet } from 'react-router'
import Header from '@/components/header'
import Footer from '@/components/footer'
import ProgressBar from '@/components/commom/backToTop'
import languageInitializer from '@/utils/languageInitializer'
// import SmoothScrollContainer from '@/components/commom/backToTop/smoothscroll'

export default function Layout() {
  languageInitializer()

  return (
    <div
      // ref={refMain}
      className='min-h-screen p-0 m-0 relative overflow-hidden bg-[rgb(12,_12,_24)] antialiased '
    >
      <Header />
      <div className='main min-h-[60vh]'>
        <Outlet></Outlet>
      </div>
      <Footer />
      <ProgressBar />
      {/* <SmoothScrollContainer /> */}
    </div>
  )
}
