import i18n from '@/i18n/i18n'

const logo_bg = '/img-home/bg-logo.png'
const logo_white = '/images/logo-white.png'
const logo_main = '/images/logo.png'
const logo_mb = '/images/logo-mb.png'
const banner_home_1 = '/img-home/Background-bn.png'
const banner_home_2 = '/img-home/banner-home-1.jpg'
const banner_home_3 = '/img-home/banner-home-2.jpg'
const banner_ln = '/images/bg-linear.png'
const banner_ln_2 = '/images/bg-linear-2.png'
const banner_ln_3 = '/images/bg-linear-3.png'
const banner_ln_4 = 'images/bg-linear-4.png'
const wm_dc = '/images/wm-dc.png'
const wm_dc_2 = '/images/wm-dc-2.png'
const wm_dc_3 = '/images/wm-dc-3.png'
const wm_dc_4 = '/images/wm-dc-4.png'
const product_intro_img = '/images/product-intro-img.png'
const product_intro_img_1 = '/images/product-intro-img-1.png'
const bg_logo_fade = '/images/bg-logo-fade.png'
const chip_icon = '/images/chip-icon.png'
const idea_icon = '/images/idea-icon.png'
const search_form_ic = '/images/Search-icon.png'
const chevronLeft = '/images/ChevronLeft.png'
const next_btn = '/images/next-btn.png'
const close_icon = '/images/Close-icon.png'
const toggle_icon = '/images/toggle-icon.png'
const girl_img = '/images/girl-find.png'
const not_found = '/images/notfound.png'
const not_found_2 = '/images/notfound2.png'

export const images = {
  logo_bg,
  logo_white,
  logo_main,
  banner_home_1,
  banner_ln,
  wm_dc,
  product_intro_img,
  bg_logo_fade,
  chip_icon,
  product_intro_img_1,
  idea_icon,
  search_form_ic,
  banner_ln_2,
  banner_ln_3,
  banner_ln_4,
  wm_dc_2,
  wm_dc_3,
  wm_dc_4,
  chevronLeft,
  next_btn,
  logo_mb,
  close_icon,
  toggle_icon,
  girl_img,
  not_found,
  not_found_2
}

export const dataBannerHomePage = [
  {
    image: banner_home_1,
    title: 'Chơi với Nexus'
  },
  {
    image: banner_home_2,
    title: 'Chơi với Nexus'
  },
  {
    image: banner_home_1,
    title: 'Chơi với Nexus'
  },
  {
    image: banner_home_3,
    title: 'Chơi với Nexus'
  }
]

export const dataIntroHome = [
  {
    icon: '/img-home/Api_icon.png',
    content: 'Máy poker, iGaming trực tiếp & nhiều hơnTất cả thông qua mộ'
  },
  {
    icon: '/img-home/Api_icon.png',
    content: 'Máy poker, iGaming trực tiếp & nhiều hơnTất cả thông qua mộ'
  },
  {
    icon: '/img-home/Api_icon.png',
    content: 'Máy poker, iGaming trực tiếp & nhiều hơnTất cả thông qua mộ'
  },
  {
    icon: '/img-home/Api_icon.png',
    content: 'Máy poker, iGaming trực tiếp & nhiều hơnTất cả thông qua mộ'
  }
]

export const dataNewsHome = [
  {
    image: banner_home_1,
    title: 'NEXUS Bolsters zambia hợp tác với iGaming trực tiếp bằng lòng',
    content:
      'Nhà cung cấp nhận iGaming trực tiếp Các trò chơi sống với Zambia test test test test test test test test test test test test test test test',
    link: '/',
    date: '12 T2 2024'
  },
  {
    image: '',
    title: 'NEXUS Bolsters zambia hợp tác với iGaming trực tiếp bằng lòng',
    content: 'Nhà cung cấp nhận iGaming trực tiếp Các trò chơi sống với Zambia vbbbfbfb',
    link: '/',
    date: '12 T2 2024'
  },
  {
    image: '',
    title: 'NEXUS đi về phía bắc để tìm kiếm Sòng bạc Wynn.',
    content: 'Nhà cung cấp nhận iGaming trực tiếp Các trò chơi sống với Zambia vbbbfbfb',
    link: '/',
    date: '12 T2 2024'
  },
  {
    image: '',
    title: 'NEXUS Bolsters zambia hợp tác với iGaming trực tiếp bằng lòng',
    content: 'Nhà cung cấp nhận iGaming trực tiếp Các trò chơi sống với Zambia vbbbfbfb',
    link: '/',
    date: '12 T2 2024'
  },
  {
    image: '',
    title: 'Test',
    content: 'Nhà cung cấp nhận iGaming trực tiếp Các trò chơi sống với Zambia vbbbfbfb',
    link: '/',
    date: '12 T2 2024'
  }
]

export const dataFeature = [
  {
    image: chip_icon,
    content: `productSectionThree.subOne`
  },
  {
    image: chip_icon,
    content: `productSectionThree.subTwo`
  },
  {
    image: chip_icon,
    content: `productSectionThree.subThree`
  },
  {
    image: chip_icon,
    content: `productSectionThree.subFour`
  },
  {
    image: chip_icon,
    content: `productSectionThree.subFive`
  },
  {
    image: chip_icon,
    content: `productSectionThree.subSix`
  },
  {
    image: chip_icon,
    content: `productSectionThree.subSeven`
  },
  {
    image: chip_icon,
    content: `productSectionThree.subEight`
  }
]

export const dataReputation = [
  {
    icon: idea_icon,
    title: `companySectionThree.card1.title`,
    content: 'companySectionThree.card1.desc'
  },
  {
    icon: idea_icon,
    title: `companySectionThree.card2.title`,
    content: 'companySectionThree.card2.desc'
  },
  {
    icon: idea_icon,
    title: `companySectionThree.card3.title`,
    content: 'companySectionThree.card2.desc'
  }
]

export const rightsData = [
  {
    icon: search_form_ic,
    title: 'companySectionFour.row1.title',
    content: 'companySectionFour.row1.description'
  },
  {
    icon: search_form_ic,
    title: 'companySectionFour.row2.title',
    content: 'companySectionFour.row1.description'
  },
  {
    icon: search_form_ic,
    title: 'companySectionFour.row3.title',
    content: 'companySectionFour.row1.description'
  }
]

export const dataNews = [
  {
    title: 'Nexus Bolsters đối tác với nội dung iGaming trực tiếp',
    date: '23rd Th1 2024'
  },
  {
    title: 'Nexus Bolsters đối tác với nội dung iGaming trực tiếp',
    date: '23rd Th1 2024'
  },
  {
    title: 'Nexus Bolsters đối tác với nội dung iGaming trực tiếp',
    date: '23rd Th1 2024'
  },
  {
    title: 'Nexus Bolsters đối tác với nội dung iGaming trực tiếp',
    date: '23rd Th1 2024'
  },
  {
    title: 'Nexus Bolsters đối tác với nội dung iGaming trực tiếp',
    date: '23rd Th1 2024'
  },
  {
    title: 'Nexus Bolsters đối tác với nội dung iGaming trực tiếp',
    date: '23rd Th1 2024'
  }
]
