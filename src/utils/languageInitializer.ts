import { useEffect } from 'react'
import i18n from '../i18n/i18n'

const languageInitializer = () => {
  useEffect(() => {
    const currentLang = sessionStorage.getItem('currentLang') || 'en'
    i18n.changeLanguage(currentLang)
  }, [])
}

export default languageInitializer
