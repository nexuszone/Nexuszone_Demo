import { useTranslation } from 'react-i18next'

interface data {
  image?: string
  content?: string
}

const FeatureBox = (data: data) => {
  const { t } = useTranslation()
  return (
    <div className=''>
      <div className='h-full flex'>
        <div className='max-md:w-[12rem] max-md:min-h-[12rem] w-[15rem] min-h-[15rem] bg-purple-dark flex items-center justify-center rounded-[0.5rem]'>
          <img className='max-sm:w-[5rem]' src={data?.image} alt='' />
        </div>
        <div className='max-lg:p-[2rem] flex-1 flex items-center p-[3rem]'>
          <p className='text-white font-bold text-[1.4rem]'>{t(`${data?.content}`)}</p>
        </div>
      </div>
    </div>
  )
}

export default FeatureBox
