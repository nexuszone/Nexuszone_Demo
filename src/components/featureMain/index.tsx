import { dataFeature } from '@/assets/data'
import FeatureBox from './featureBox'
import { useTranslation } from 'react-i18next'

const FeatureMain = () => {
  // const datas = dataFeature

  const { t } = useTranslation()

  return (
    <div className='max-lg:py-[2rem] py-[9.6rem]'>
      <div className='container'>
        <h2 className='max-lg:mb-[1rem] text-white font-bold text-[3rem] mb-[3.2rem]'>
          {t('productSectionThree.title')}
        </h2>
        <p className='max-lg:mb-[3rem] text-gray-1 mb-[7.2rem]'>
          {t('productSectionThree.subTitle')}
        </p>

        <div>
          <div className='max-lg:gap-[2rem] flex flex-wrap gap-[3rem]'>
            {dataFeature &&
              dataFeature.map((item, index) => {
                return (
                  <div className='max-md:basis-[50%] flex-1 shrink-0 basis-[40%]' key={index}>
                    <FeatureBox {...item} />
                  </div>
                )
              })}
          </div>
        </div>
      </div>
    </div>
  )
}

export default FeatureMain
