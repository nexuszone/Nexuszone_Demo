import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/effect-fade'
import 'swiper/css/pagination'

import { dataReputation } from '@/assets/data'
import ReputationItem from './reputationItem'
import { useMediaQuery } from 'usehooks-ts'

const Reputation = () => {
  const matches = useMediaQuery('(max-width: 768px')

  return (
    <div className='max-lg:py-[2rem] max-md:py-[4rem] py-[10rem]'>
      <div className='container'>
        <div className='max-lg:gap-[2rem] flex flex-wrap gap-[8rem]'>
          {matches ? (
            <Swiper
              spaceBetween={20}
              slidesPerView={3}
              speed={1000}
              pagination={{
                clickable: true
              }}
              breakpoints={{
                0: {
                  slidesPerView: 1.2
                },
                375: {
                  slidesPerView: 1.2
                },
                480: {
                  slidesPerView: 1.5
                },
                720: {
                  slidesPerView: 2.5
                },
                900: {
                  slidesPerView: 3
                },
                1200: {
                  slidesPerView: 3
                }
              }}
            >
              {dataReputation &&
                dataReputation.map((item, index) => {
                  return (
                    // <div className='flex-1'>
                    //   <ReputationItem {...item} />
                    // </div>
                    <SwiperSlide key={index}>
                      <ReputationItem {...item} />
                    </SwiperSlide>
                  )
                })}
            </Swiper>
          ) : (
            <>
              {dataReputation &&
                dataReputation.map((item, index) => {
                  return (
                    <div className='flex-1' key={index}>
                      <ReputationItem {...item} />
                    </div>
                  )
                })}
            </>
          )}
        </div>
      </div>
    </div>
  )
}

export default Reputation
