import { images } from '@/assets/data'
import { useTranslation } from 'react-i18next'

interface data {
  icon: string
  title: string
  content: string
}

const ReputationItem = (item: data) => {
  const { logo_white } = images
  const { t } = useTranslation()
  return (
    <div className='max-md:pb-[1.5rem] max-md:pt-[1.5rem] max-md:px-[2rem] bg-purple-dark rounded-[0.5rem] pt-[3.6rem] pb-[4.2rem] px-[2.4rem] flex flex-col items-center h-full'>
      <div className='max-sm:w-[5rem] max-sm:mb-[1.5rem] mb-[2.4rem] w-[7.8rem]'>
        <img src={item?.icon} alt='' />
      </div>
      <span className='max-sm:mb-[1.5rem] block w-fit mx-auto mb-[2.4rem] opacity-50'>
        <img src={logo_white} className='max-sm:w-[3rem] w-[5rem]' alt='' />
      </span>
      <div className='text-center'>
        <h4 className='max-sm:text-[2rem] text-white text-[2.2rem] font-bold mb-[1rem]'>
          {t(`${item?.title}`)}
        </h4>
        <p className='text-gray-1'>{t(`${item?.content}`)}</p>
      </div>
    </div>
  )
}

export default ReputationItem
