import { cn } from '@/utils/cn'
import { useNavigate } from 'react-router'
import './card.css'
import React from 'react'
import { Button } from '../ui/button'
import { IconCloverAlt } from '@/icon'

interface CardProps {
  imgBg?: string
  title?: string
  cap?: string
  href: string
  titleButton?: string
}

export const Card: React.FC<CardProps> = ({
  href,
  cap,
  imgBg = 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/Han_So-hee_Airport_Departure_July_2023_20.jpg/800px-Han_So-hee_Airport_Departure_July_2023_20.jpg',
  title,
  titleButton = 'PLAY GAME'
}) => {
  const router = useNavigate()

  return (
    <div
      onClick={() => router(href)}
      style={{
        borderRadius: '10px'
      }}
      className={cn(
        'cursor-pointer group relative max-w-[260px] overflow-clip  max-h-[260px] flex flex-col justify-end w-full h-screen card hover:shadow-lg'
      )}
    >
      <div
        className='w-full h-full transition-transform absolute top-0 left-0  duration-[5s] transform-gpu hover:scale-125 bg-cover bg-center z-5  overflow-hidden'
        style={{ backgroundImage: `url('${imgBg}')`, borderRadius: '10px' }}
      ></div>
      <div className='h-[145%] group-hover:-translate-y-2 translate-y-[20px] group-hover:opacity-100 duration-300 group-hover:card-overlay-hover absolute  w-[100%] top-0 left-0 bg-gradient-to-b from-transparent via-black-80 to-black'></div>
      <div className='flex flex-col gap-[14px] px-[35px] py-[35px] relative z-3'>
        <div className='text-white text-[12px] font-medium uppercase group-hover:-translate-y-6 group-hover:duration-700 duration-1000  text-nowrap line-clamp-1 transform translate-y-[60px] card-small-title'>
          {cap}
        </div>
        <div className='text-white text-nowrap line-clamp-1 text-[22px] font-medium group-hover:-translate-y-6 duration-1000  transform translate-y-[60px] card-title'>
          {title}
        </div>
        <div className='opacity-0 group-hover:opacity-100 group-hover:-translate-y-2 duration-700 translate-y-[74px] card-button'>
          <Button className='flex group/button items-center max-w-max px-4 py-6  bg-white hover:bg-[#45485f] transition-all duration-200 ease-in-out'>
            <IconCloverAlt className='w-6 h-6 mr-2' />
            <span className='text-[16px] font-bold text-[#45485f] uppercase group-hover/button:text-white'>
              {titleButton}
            </span>
          </Button>
        </div>
      </div>
    </div>
  )
}
