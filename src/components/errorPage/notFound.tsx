import { images } from '@/assets/data'

const NotFound = () => {
  const { not_found, not_found_2 } = images
  return (
    <div className='py-[10rem] relative'>
      <div className='container'>
        <div className='flex'>
          <div className='relative w-full h-full'>
            <div className='absolute z-10 top-0 pointer-events-none'>
              <svg
                viewBox='0 0 450 50'
                className='[font:bold_6rem_Century_Gothic,_Arial] w-full h-[39rem]'
              >
                <text y='25' fill='none' className='stroke-[white] stroke-[.05rem] uppercase'>
                  404 Page
                </text>
                <text y='85' fill='none' className='stroke-[white] stroke-[.05rem] uppercase'>
                  Not found
                </text>
              </svg>
            </div>
          </div>
        </div>
        <div className='w-2/5 ml-auto relative mr-[-5rem] '>
          <img src={not_found_2} className='w-full' alt='' />
        </div>
      </div>
    </div>
  )
}

export default NotFound
