import { useTranslation } from 'react-i18next'
interface data {
  icon?: string
  title?: string
  content?: string
}

const RightsItem = (item: data) => {
  const { t } = useTranslation()
  return (
    <div className=''>
      <div className='flex items-center mb-[2.4rem] gap-[0.8rem]'>
        <span className='block w-[4.8rem]'>
          <img src={item?.icon} className='max-sm:w-[3.5rem] object-contain' alt='' />
        </span>
        <span className='max-sm:text-[2.2rem] font-bold text-[3rem]'>{t(`${item?.title}`)}</span>
      </div>
      <p className='text-[#5c5c7e]'>{t(`${item?.content}`)}</p>
    </div>
  )
}

export default RightsItem
