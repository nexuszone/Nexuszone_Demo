import { rightsData } from '@/assets/data'
import RightsItem from './RightsItem'

const RightSection = () => {
  return (
    <div className='max-md:py-[4rem] py-[9.6rem] bg-white'>
      <div className='container'>
        <div className='flex flex-col  gap-y-[3.2rem]'>
          {rightsData &&
            rightsData.map((item, index) => {
              return (
                <div key={index}>
                  <RightsItem {...item} />
                </div>
              )
            })}
        </div>
      </div>
    </div>
  )
}

export default RightSection
