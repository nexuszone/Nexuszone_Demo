import { cn } from '@/utils/cn'
import { Fragment } from 'react'
import { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Button from '@/components/commom/buttons/Button'
import sendMail from '@/icon/sendMail'
import { images } from '@/assets/data'
import i18n from '../../i18n/i18n'
import { useTranslation } from 'react-i18next'
import IconYoutube from '@/icon/youtube'
import { IconTelegram } from '@/icon/telegram'
import IconXTW from '@/icon/xtw'
import { IconFacebook } from '@/icon/facebook'
import IconArrowDown from '@/icon/arrowDown'

const Header = () => {
  const [activeScroll, setActiveScroll] = useState<boolean>(false)
  const { logo_main, logo_mb, close_icon } = images
  const { t } = useTranslation()
  const navigate = useNavigate()
  const [toggle, setToggle] = useState({
    isOpen1: false,
    isOpen2: false,
    isOpen3: false
  })
  const [langbtn, setBtn] = useState<boolean>(false)
  const [mbhd, setMbhd] = useState<boolean>(false)
  const { toggle_icon } = images
  const [lang, setLang] = useState<string>(i18n.language)

  const handleChangeLanguage = (lang: string) => {
    i18n.changeLanguage(lang)
    setLang(lang)
    sessionStorage.setItem('currentLang', lang)
    setBtn(false)
  }

  useEffect(() => {
    const handleScroll = function (_e: Event) {
      if (window.scrollY > 0 || document.documentElement.scrollTop > 0) {
        setActiveScroll(true)
      } else {
        setActiveScroll(false)
      }
    }

    setLang(sessionStorage.getItem('currentLang') + '')
    window.addEventListener('scroll', handleScroll)

    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])

  const handleScrollTo = () => {
    window.scrollTo({
      top: 0
    })
  }

  const handleUrl = () => {
    setMbhd(false)
    handleScrollTo()
  }

  const handleTogleMenu = () => {
    setMbhd(!mbhd)
    setBtn(false)

    if (!mbhd) return (document.body.style.overflow = 'hidden')

    document.body.style.overflow = ''
  }

  return (
    <Fragment>
      <header
        className={cn(
          'header fixed w-full z-[1000] before:absolute before:content-[""] before:z-[-1] before:[transition:all_0.3s_ease-in-out] before:w-full before:h-full before:top-[0] before:left-[0] before:bg-[#0000003d] before:backdrop-filter before:backdrop-blur-[2.7rem] before:-translate-y-full',
          activeScroll && 'before:translate-y-0'
        )}
      >
        <div className='container max-w-[1801px]'>
          <div className='header-wrap flex items-center justify-between max-lg:py-[1.6rem]'>
            <Link to='/' onClick={() => handleUrl()}>
              <img src={logo_main} alt='' />
            </Link>
            <div
              className={cn(
                'max-lg:fixed max-lg:flex max-lg:top-0 max-lg:bottom-0 max-lg:left-0  max-lg:w-full max-lg:bg-black-1 max-lg:py-[2rem] max-lg:px-[2.4rem] max-lg:flex-col lg:mx-auto max-lg:translate-x-[-105%] [transition:all_0.3s_ease-in-out] z-20',
                mbhd && 'max-lg:translate-x-0 max-lg:[transition:all_0.5s_ease-in-out]'
              )}
            >
              <div className='hidden max-lg:flex max-lg:items-center gap-[4.5rem] mb-[3.2rem]'>
                <Link
                  to='/'
                  className='max-lg:block max-lg:shrink-0 w-[4rem]'
                  onClick={() => handleUrl()}
                >
                  <img src={logo_mb} className='w-full object-contain' alt='' />
                </Link>

                <div className='header-language max-lg:ml-auto' onClick={() => setBtn(!langbtn)}>
                  <span className='block w-[2.4rem] h-[2.4rem]'>
                    <img src='/images/languageIcon.png' className='w-full object-contain' alt='' />
                  </span>
                </div>
                <span
                  className='block w-[2rem] h-[2rem] cursor-pointer'
                  onClick={() => handleTogleMenu()}
                >
                  <img src={close_icon} className='object-contain' alt='' />
                </span>
              </div>
              <div className='header-nav flex max-lg:flex-col gap-[4rem] mx-auto max-lg:gap-[1.8rem] max-lg:overflow-y-auto max-lg:w-full'>
                <div className='relative group max-lg:bg-black-2 max-lg:rounded-[0.8rem]'>
                  <Link
                    to='/'
                    className='relative flex justify-between gap-[1rem] py-[2.5rem] max-lg:py-[1.8rem] max-lg:px-[2rem] text-white text-[1.4rem] font-bold before:absolute before:content-[""] before:w-full before:h-[2] before:top-full [transition:all_0.2s_ease-in-out] lg:hover:text-[#e02a87]'
                    onClick={() => handleUrl()}
                  >
                    {t('header.home')}
                  </Link>
                </div>
                <div className='relative group max-lg:bg-black-2 max-lg:rounded-[0.8rem]'>
                  <Link
                    to='#'
                    className={cn(
                      'relative py-[2.5rem] max-lg:py-[1.8rem] max-lg:px-[2rem] text-white text-[1.4rem] font-bold block [transition:all_0.2s_ease-in-out] lg:hover:text-[#e02a87]',
                      toggle.isOpen2 && 'text-[#e02a87]'
                    )}
                  >
                    {t('header.products.title')}

                    <span
                      className='w-[3.2rem] h-[3.2rem] hidden max-lg:flex max-lg:items-center max-lg:justify-center absolute top-1/2 right-[1.8rem] -translate-y-1/2'
                      onClick={(e) => {
                        e.preventDefault()
                        setToggle({ ...toggle, isOpen2: !toggle.isOpen2 })
                      }}
                    >
                      <IconArrowDown
                        className={cn(
                          'w-[1.6rem] h-[1.6rem] [transition:all_0.2s_ease-in-out] -rotate-90',
                          toggle.isOpen2 && 'rotate-0'
                        )}
                      />
                    </span>
                  </Link>
                  <ul
                    className={cn(
                      'absolute min-w-[15rem] bg-purple-1 left-1/2  top-[calc(100%-1rem)] -translate-x-1/2 rounded-[1rem] overflow-hidden z-10 opacity-0 [transition:all_0.2s_ease-in-out] pointer-events-none group-hover:opacity-100 group-hover:top-full group-hover:pointer-events-auto max-lg:static max-lg:opacity-100 max-lg:pointer-events-auto max-lg:transform-none max-lg:bg-transparent max-lg:max-h-[0rem] max-lg:[transition:all_0.3s_ease-in-out]',
                      toggle.isOpen2 &&
                        'max-lg:max-h-[30rem] max-lg:[transition:all_0.6s_ease-in-out]'
                    )}
                  >
                    <li>
                      <Link
                        className='text-gray-1 py-[1.2rem] px-[1.6rem] block [transition:all_0.2s_ease-in-out] lg:hover:text-white lg:hover:bg-purple-2'
                        to='/product'
                        onClick={() => handleUrl()}
                      >
                        {t('header.products.subTitle1')}
                      </Link>
                    </li>
                    <li>
                      <Link
                        className='text-gray-1 py-[1.2rem] px-[1.6rem] block [transition:all_0.2s_ease-in-out] lg:hover:text-white lg:hover:bg-purple-2'
                        to='/'
                        onClick={() => handleUrl()}
                      >
                        {t('header.products.subTitle2')}
                      </Link>
                    </li>
                  </ul>
                </div>
                <div className='relative group max-lg:bg-black-2 max-lg:rounded-[0.8rem]'>
                  <Link
                    to='#'
                    className={cn(
                      'relative flex justify-between gap-[1rem] py-[2.5rem] max-lg:py-[1.8rem] max-lg:px-[2rem] text-white text-[1.4rem] font-bold before:absolute before:content-[""] before:w-full before:h-[2] before:top-full [transition:all_0.2s_ease-in-out] lg:hover:text-[#e02a87]',
                      toggle.isOpen1 && 'text-[#e02a87]'
                    )}
                  >
                    {t('header.company.title')}

                    <span
                      className='w-[3.2rem] h-[3.2rem] hidden max-lg:flex max-lg:items-center max-lg:justify-center absolute top-1/2 right-[1.8rem] -translate-y-1/2'
                      onClick={(e) => {
                        e.preventDefault()
                        setToggle({ ...toggle, isOpen1: !toggle.isOpen1 })
                      }}
                    >
                      <IconArrowDown
                        className={cn(
                          'w-[1.6rem] h-[1.6rem] [transition:all_0.2s_ease-in-out] -rotate-90',
                          toggle.isOpen1 && 'rotate-0'
                        )}
                      />
                    </span>
                  </Link>

                  <ul
                    className={cn(
                      'absolute min-w-[15rem] bg-purple-1 left-1/2  top-[calc(100%-1rem)] -translate-x-1/2 rounded-[1rem] overflow-hidden z-10 opacity-0 [transition:all_0.2s_ease-in-out] pointer-events-none group-hover:opacity-100 group-hover:top-full group-hover:pointer-events-auto max-lg:static max-lg:opacity-100 max-lg:pointer-events-auto max-lg:transform-none max-lg:bg-transparent max-lg:max-h-[0rem] max-lg:[transition:all_0.3s_ease-in-out]',
                      toggle.isOpen1 &&
                        'max-lg:max-h-[30rem] max-lg:[transition:all_0.6s_ease-in-out]'
                    )}
                  >
                    <li>
                      <Link
                        className='text-gray-1 py-[1.2rem] px-[1.6rem] block [transition:all_0.2s_ease-in-out] lg:hover:text-white lg:hover:bg-purple-2'
                        to='/company'
                        onClick={() => handleUrl()}
                      >
                        {t('header.company.subTitle1')}
                      </Link>
                    </li>
                    <li>
                      <Link
                        className='text-gray-1 py-[1.2rem] px-[1.6rem] block [transition:all_0.2s_ease-in-out] lg:hover:text-white lg:hover:bg-purple-2'
                        to='/'
                        onClick={() => handleUrl()}
                      >
                        {t('header.company.subTitle2')}
                      </Link>
                    </li>
                  </ul>
                </div>
                <div className='relative group max-lg:bg-black-2 max-lg:rounded-[0.8rem]'>
                  <Link
                    to='/news'
                    className='py-[2.5rem] max-lg:py-[1.8rem] max-lg:px-[2rem] text-white text-[1.4rem] font-bold block [transition:all_0.2s_ease-in-out] lg:hover:text-[#e02a87]'
                    onClick={() => handleUrl()}
                  >
                    {t('header.news.title')}
                  </Link>
                </div>
                <div className='relative group max-lg:bg-black-2 max-lg:rounded-[0.8rem]'>
                  <Link
                    to='/contact'
                    className='py-[2.5rem] max-lg:py-[1.8rem] max-lg:px-[2rem] text-white text-[1.4rem] font-bold block [transition:all_0.2s_ease-in-out] lg:hover:text-[#e02a87]'
                    onClick={() => handleUrl()}
                  >
                    {t('header.contact.title')}
                  </Link>
                </div>
              </div>
              <div className='hidden max-lg:block max-lg:pt-[4rem] mt-auto max-lg:w-full'>
                <p className='text-white mb-[2.4rem]'>Social media:</p>

                <div className='flex gap-[2rem] items-center'>
                  <Link to='/'>
                    <IconFacebook className='w-[2.4rem] h-[2.4rem]' />
                  </Link>
                  <Link to='/'>
                    <IconTelegram className='w-[2.4rem] h-[2.4rem]' />
                  </Link>
                  <Link to='/'>
                    <IconXTW className='w-[2.4rem] h-[2.4rem]' />
                  </Link>
                  <Link to='/'>
                    <IconYoutube className='w-[2.4rem] h-[2.4rem]' />
                  </Link>
                </div>
              </div>
            </div>
            <div className='relative max-lg:hidden group py-[2.4rem] cursor-pointer'>
              <span className='flex items-center justify-center mr-[24px] w-[2.4rem] h-[2.4rem]'>
                <img src='/images/languageIcon.png' className='w-full' alt='' />
              </span>
              <ul className='absolute min-w-[15rem] bg-purple-1 rounded-[1rem] right-[-0rem] top-[calc(100%-2rem)] overflow-hidden z-10 opacity-0 [transition:all_0.2s_ease-in-out] pointer-events-none group-hover:opacity-100 group-hover:top-[calc(100%)] group-hover:pointer-events-auto max-lg:static max-lg:opacity-100 max-lg:pointer-events-auto max-lg:transform-none max-lg:bg-transparent max-lg:max-h-[0rem] max-lg:[transition:all_0.3s_ease-in-out]'>
                <li
                  className='cursor-pointer text-gray-1 py-[1.2rem] px-[1.6rem] block [transition:all_0.2s_ease-in-out] lg:hover:text-white lg:hover:bg-purple-2'
                  onClick={() => handleChangeLanguage('en')}
                >
                  English
                </li>
                <li
                  className='cursor-pointer text-gray-1 py-[1.2rem] px-[1.6rem] block [transition:all_0.2s_ease-in-out] lg:hover:text-white lg:hover:bg-purple-2'
                  onClick={() => handleChangeLanguage('vi')}
                >
                  Tiếng việt
                </li>
              </ul>
            </div>
            <Button
              variant='icBdWhite'
              title={`${t('header.btnContact')}`}
              iconStart={sendMail}
              onClick={() => {
                navigate('/contact')
                handleUrl()
              }}
              className='max-lg:hidden'
            />
            <div
              className={cn(
                'cursor-pointer w-[3rem] hidden max-lg:block [transition:all_0.2s_ease-in-out]',
                mbhd && 'pointer-events-none opacity-50'
              )}
              onClick={() => handleTogleMenu()}
            >
              <img src={toggle_icon} className='w-full h-full object-contain' alt='' />
            </div>
          </div>
        </div>
      </header>
      <span
        className={cn(
          'max-lg:fixed max-lg:w-full max-lg:h-full max-lg:left-0 max-lg:top-0 max-lg:translate-x-[-105%]  max-lg:bg-[rgb(0,_0_,0,_70%)] max-lg:z-[999] max-lg:[transition:all_0.5s_ease-in-out] max-sm:[transition:all_0.5s_ease-in-out]',
          mbhd &&
            'max-lg:translate-x-0 max-lg:[transition:all_0.3s_ease-in-out] max-sm:[transition:all_0.3s_ease-in-out]'
        )}
        onClick={() => setMbhd(false)}
      ></span>
      <div
        className={cn(
          'fixed lg:hidden z-[1001] w-full bottom-0 bg-[#18182c] pb-[4rem] pt-[3rem] rounded-tl-[2.5rem] rounded-tr-[1.6rem] translate-y-full [transition:all_0.3s_ease-in-out]',
          langbtn && 'translate-y-0'
        )}
      >
        <p className='text-white px-[1.6rem] mb-[1rem]'>Ngôn ngữ</p>
        <ul className='text-white'>
          <li
            className={cn(
              'flex justify-between items-center p-[1.6rem] cursor-pointer',
              lang === 'en' && 'bg-[#2c2c42]'
            )}
            onClick={() => handleChangeLanguage('en')}
          >
            English <span className='block'>&#x2714;</span>
          </li>
          <li
            className={cn(
              'flex justify-between items-center p-[1.6rem] cursor-pointer',
              lang === 'vi' && 'bg-[#2c2c42]'
            )}
            onClick={() => handleChangeLanguage('vi')}
          >
            Tiếng việt <span className='block'>&#x2714;</span>
          </li>
        </ul>
      </div>
    </Fragment>
  )
}

export default Header
