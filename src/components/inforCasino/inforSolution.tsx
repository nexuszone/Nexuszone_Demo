import { images } from '@/assets/data'
import { useTranslation } from 'react-i18next'

const InforSolution = () => {
  const { product_intro_img_1, bg_logo_fade } = images

  const { t } = useTranslation()

  return (
    <div className='max-md:py-[4rem] py-[6.4rem] bg-white'>
      <div className='container'>
        <div className='max-md:flex-col-reverse flex mx-[-1.5rem] flex-wrap items-center'>
          <div className='max-md:w-full px-[1.5rem] w-1/2'>
            <div className='max-md:max-w-full max-w-[64.5rem] relative'>
              <h3 className='max-md:mb-[1rem] max-md:mt-[2rem] text-[30px] font-bold mb-[3.2rem]'>
                {t('productSectionFour.title')}
              </h3>
              <p className='max-md:mb-[1rem] font-bold mb-[2rem]'>
                {' '}
                {t('productSectionFour.subTitle')}
              </p>
              <p className='text-[#5c5c7e] text-[1.6rem]'>{t('productSectionFour.description')}</p>
            </div>
          </div>
          <div className='max-md:w-full px-[1.5rem] w-1/2 relative'>
            <img
              src={product_intro_img_1}
              className='max-md:max-w-[80%] relative z-10 mx-auto'
              alt=''
            />
            <div className='max-md:pl-[1.5rem] max-md:pr-[1.5rem] absolute w-full top-0 bottom-0 left-1/2 -translate-x-1/2'>
              <img src={bg_logo_fade} className='h-full mx-auto' alt='' />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default InforSolution
