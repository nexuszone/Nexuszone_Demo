import { images } from '@/assets/data'
import { useTranslation } from 'react-i18next'

const InforCasino = () => {
  const { product_intro_img, bg_logo_fade } = images

  const { t } = useTranslation()

  return (
    <div className='max-md:pb-[4rem] py-[4.8rem] bg-white'>
      <div className='container'>
        <div className='max-md:flex-col flex mx-[-1.5rem] items-center gap-6'>
          <div className='max-lg:w-full max-lg:flex max-lg:items-center max-lg:justify-center max-lg:mx-[auto] max-lg:my-[0] px-[1.5rem] w-1/2 relative'>
            <img className='max-md:max-w-[80%]' src={product_intro_img} alt='' />
            <div className='max-lg:left-[1.5rem] max-lg:right-[1.5rem] max-lg:mx-[auto] max-lg:my-[0] max-lg:flex max-lg:items-center max-lg:justify-center max-md:mt-[4rem] absolute top-[-4.8rem] bottom-[-4.8rem] left-0'>
              <img src={bg_logo_fade} alt='' />
            </div>
          </div>
          <div className='max-lg:w-full max-lg:mt-[6rem] px-[1.5rem] w-1/2'>
            <div className='max-w-[64.5rem] relative'>
              <h3 className='max-lg:mb-[1rem] text-[30px] font-bold mb-[3.2rem]'>
                {t('productSectionTwo.title')}
              </h3>
              <p className='text-[#5c5c7e] text-[1.6rem]'>{t('productSectionTwo.subTitle')}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default InforCasino
