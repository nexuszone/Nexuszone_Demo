import { images } from '@/assets/data'
import { useTranslation } from 'react-i18next'

const InforMission = () => {
  const { bg_logo_fade } = images

  const { t } = useTranslation()

  return (
    <div className='max-md:pt-[0rem] max-md:pb-[4rem] py-[8rem] bg-white'>
      <div className='container'>
        <div className='max-md:flex-col flex mx-[-1.5rem] flex-wrap items-center'>
          <div className='max-md:w-full max-md:flex max-md:items-center max-md:justify-center max-md:mx-[auto] max-md:my-[0] px-[1.5rem] w-1/2 relative min-h-[30rem] flex items-center'>
            <p className='max-sm:text-[3rem] max-sm:text-center uppercase font-black text-[5rem] relative z-10 max-w-[38rem] leading-none my-auto'>
              {t('companySectionTwo.left')}
            </p>
            <div className='max-lg:left-[1.5rem] max-lg:right-[1.5rem] max-lg:mx-[auto] max-lg:my-[0] max-lg:flex max-lg:items-center max-lg:justify-center max-md:mt-[4rem] absolute top-[-8rem] bottom-[-8rem] left-0 pointer-events-none'>
              <img src={bg_logo_fade} className='max-md:max-w-[100%] ' alt='' />
            </div>
          </div>
          <div className='max-md:w-full px-[1.5rem] w-1/2'>
            <div className='max-md:mt-[3rem] max-w-[64.5rem] relative'>
              <h3 className='max-sm:text-[1.9rem] max-md:mb-[1rem] text-[30px] font-bold mb-[3rem] uppercase'>
                {t('companySectionTwo.rightSubTitle')}
              </h3>
              <h4 className='uppercase text-[1.8rem] font-bold mb-[0.8rem]'>
                {t('companySectionTwo.rightSubTitle1')}
              </h4>
              <p className='text-[#5c5c7e] text-[1.6rem]'>{t('companySectionTwo.rightDesc1')}</p>
              <div className='max-md:mb-[1rem] mb-[3rem]'></div>
              <h4 className='uppercase text-[1.8rem] font-bold mb-[0.8rem]'>
                {t('companySectionTwo.rightSubTitle2')}
              </h4>
              <p className='text-[#5c5c7e] text-[1.6rem]'>{t('companySectionTwo.rightDesc2')}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default InforMission
