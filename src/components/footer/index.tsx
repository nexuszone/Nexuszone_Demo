import { images } from '@/assets/data'
import { Link } from 'react-router-dom'
import { IconFacebook } from '@/icon/facebook'
import { IconTelegram } from '@/icon/telegram'
import IconXTW from '@/icon/xtw'
import IconYoutube from '@/icon/youtube'
import { useMediaQuery } from 'usehooks-ts'
import { useTranslation } from 'react-i18next'

const Footer = () => {
  const matches = useMediaQuery('(max-width: 768px)')
  const { logo_main, logo_white } = images

  const { t } = useTranslation()

  return (
    <>
      {matches ? (
        <div className='bg-purple-dark h-[112px] flex flex-col items-center justify-center'>
          <Link to='/'>
            <img className=' bn-logo w-[100px] mx-auto' src={logo_white} alt='' />
          </Link>
        </div>
      ) : (
        <footer className='w-full bg-transparent text-gray-1 border-t-[1px] border-gray-800 pt-[4.8rem] pb-[4.8rem]'>
          <div className='container'>
            <div className='flex flex-wrap gap-6 items-start justify-between mb-[8.8rem]'>
              <div className=''>
                <Link to='/' className='w-[10.8rem] mb-[1.6rem] block'>
                  <img src={logo_main} alt='' />
                </Link>
                <p className='text-[1.1rem]'>{t('footer.copyRight')}</p>
              </div>
              <div className='flex gap-[2rem] items-center'>
                <Link to='/'>
                  <IconFacebook className='w-[2.4rem] h-[2.4rem]' />
                </Link>
                <Link to='/'>
                  <IconTelegram className='w-[2.4rem] h-[2.4rem]' />
                </Link>
                <Link to='/'>
                  <IconXTW className='w-[2.4rem] h-[2.4rem]' />
                </Link>
                <Link to='/'>
                  <IconYoutube className='w-[2.4rem] h-[2.4rem]' />
                </Link>
              </div>
            </div>
            <div className='flex flex-wra gap-[1.4rem] mb-[1.6rem]'>
              <Link className='text-gray-1 text-[1.1rem]' to='/'>
                {t('footer.terms')}
              </Link>
              <Link className='text-gray-1 text-[1.1rem]' to='/'>
                {t('footer.privacy')}
              </Link>
              <Link className='text-gray-1 text-[1.1rem]' to='/'>
                {t('footer.cookie')}
              </Link>
              <Link className='text-gray-1 text-[1.1rem]' to='/'>
                {t('footer.califomia')}
              </Link>
            </div>
            <div className='flex flex-wrap gap-[1.4rem]'>
              <Link className='text-gray-1 text-[1.1rem]' to='/'>
                {t('footer.about')}
              </Link>
              <Link className='text-gray-1 text-[1.1rem]' to='/'>
                {t('footer.support')}
              </Link>
              <Link className='text-gray-1 text-[1.1rem]' to='/'>
                {t('footer.contact')}
              </Link>
              <Link className='text-gray-1 text-[1.1rem]' to='/'>
                {t('footer.onboarding')}
              </Link>
            </div>
          </div>
        </footer>
      )}
    </>
  )
}

export default Footer
