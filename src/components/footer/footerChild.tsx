import { images } from '@/assets/data'
import { useMediaQuery } from 'usehooks-ts'

const FooterChild = () => {
  const { logo_white } = images

  const matches = useMediaQuery('(max-width: 768px')

  return (
    <>
      {!matches && (
        <div className='py-[10.4rem]'>
          <div className='container'>
            <img src={logo_white} className='mx-auto max-w-[10.4rem] object-contain' alt='' />
          </div>
        </div>
      )}
    </>
  )
}

export default FooterChild
