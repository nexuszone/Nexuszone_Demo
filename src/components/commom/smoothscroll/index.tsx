// import React, { useEffect } from 'react'

// const SmoothScroll = (target: HTMLElement) => {
//   let moving = false
//   let pos = target.scrollTop
//   let speed = 12
//   let smooth = 12
//   const frame =
//     target === document.body && document.documentElement ? document.documentElement : target // safari is the new IE

//   const scrolled = (e: WheelEvent) => {
//     e.preventDefault() // disable default scrolling

//     const delta = normalizeWheelDelta(e)

//     pos += -delta * speed
//     pos = Math.max(0, Math.min(pos, target.scrollHeight - frame.clientHeight)) // limit scrolling

//     if (!moving) update()
//   }

//   const normalizeWheelDelta = (e: WheelEvent) => {
//     if (e.detail) {
//       if (e.deltaY)
//         return (e.deltaY / e.detail / 40) * (e.detail > 0 ? 1 : -1) // Opera
//       else return -e.detail / 3 // Firefox
//     } else return e.deltaY / 120 // IE,Safari,Chrome
//   }

//   const update = () => {
//     moving = true

//     const delta = (pos - target.scrollTop) / smooth

//     target.scrollTop += delta

//     if (Math.abs(delta) > 0.5) requestFrame(update)
//     else moving = false
//   }

//   const requestFrame =
//     window.requestAnimationFrame ||
//     function (func: FrameRequestCallback) {
//       window.setTimeout(func, 1000 / 50)
//     }

//   useEffect(() => {
//     target.addEventListener('wheel', scrolled, { passive: false })
//     // target.addEventListener('DOMMouseScroll', scrolled, { passive: false });

//     return () => {
//       target.removeEventListener('wheel', scrolled)
//       //   target.removeEventListener('DOMMouseScroll', scrolled);
//     }
//   }, [target])

//   return null
// }

// const SmoothScrollContainer: React.FC = () => {
//   useEffect(() => {
//     const target =
//       document.scrollingElement ||
//       document.documentElement ||
//       document.body.parentNode ||
//       (document.body as HTMLElement)

//     if (target instanceof HTMLElement) {
//       SmoothScroll(target)
//     }
//   }, [])

//   return null
// }

// export default SmoothScrollContainer
