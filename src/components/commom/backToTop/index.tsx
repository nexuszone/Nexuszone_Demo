import React, { useEffect, useState } from 'react'

const ProgressBar: React.FC = () => {
  const [pathLength, setPathLength] = useState<number>(0)

  useEffect(() => {
    const progressPath = document.querySelector('.progress path') as SVGPathElement | null
    if (!progressPath) return

    const calculatePathLength = () => {
      const length = progressPath.getTotalLength()
      progressPath.style.transition = 'none'
      progressPath.style.strokeDasharray = `${length} ${length}`
      progressPath.style.strokeDashoffset = String(length)
      progressPath.getBoundingClientRect()
      progressPath.style.transition = 'stroke-dashoffset 10ms linear'
      setPathLength(length)
    }

    calculatePathLength()

    const updateProgress = () => {
      const scroll = window.pageYOffset
      const height = document.body.scrollHeight - window.innerHeight
      const progress = pathLength - (scroll * pathLength) / height
      if (progressPath) progressPath.style.strokeDashoffset = String(progress)
    }

    const scrollHandler = () => {
      updateProgress()
      const offset = 50
      const progressWrap = document.querySelector('.progress') as HTMLElement | null
      if (!progressWrap) return
      if (window.pageYOffset > offset) {
        progressWrap.classList.add('active-progress')
      } else {
        progressWrap.classList.remove('active-progress')
      }
    }

    window.addEventListener('scroll', scrollHandler)

    return () => {
      window.removeEventListener('scroll', scrollHandler)
    }
  }, [pathLength])

  const handleProgressBarClick = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    })
  }

  return (
    <div
      className='progress fixed bottom-12 right-[calc((100vw-143rem)/2)] mr-[max((100vw-143rem)/2+1.5rem, 1rem)] h-[4.6rem] w-[4.6rem] overflow-hidden cursor-pointer block rounded-[5rem] [box-shadow:inset_0_0_0_0.2rem_rgba(0,_0,_0,_0.1)] opacity-0 invisible translate-y-6 [transition:all_200ms_linear] z-[1000]'
      onClick={handleProgressBarClick}
    >
      <div className='progress-wrap'>
        <svg
          className='progress-circle svg-content'
          width='100%'
          height='100%'
          viewBox='-1 -1 102 102'
        >
          <path d='M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98' />
        </svg>
      </div>
    </div>
  )
}

export default ProgressBar
