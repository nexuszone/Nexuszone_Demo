import { cn } from '@/utils/cn'
import React, { ChangeEvent } from 'react'

interface InputProps {
  value?: string
  onChange?: (value: string) => void
  placeholder?: string
  type?: string
  className?: string
  name?: string
}

const Input: React.FC<InputProps> = ({ value, onChange, type, placeholder, className, name }) => {
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    // onChange(event.target.value)
  }

  return (
    <input
      type={type ? type : 'text'}
      value={value}
      onChange={handleChange}
      placeholder={placeholder}
      name={name}
      className={cn(
        'border-[0.1rem] border-transparent outline-none bg-gray-2 w-full h-[4.2rem] rounded-[0.8rem] px-[2rem]',
        className
      )}
    />
  )
}

export default Input
