import { cn } from '@/utils/cn'
import React, { ChangeEvent } from 'react'

interface TextareaProps {
  value?: string
  onChange?: (value: string) => void
  placeholder?: string
  className?: string
  name?: string
}

const Textarea: React.FC<TextareaProps> = ({ value, onChange, placeholder, className, name }) => {
  const handleChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
    // onChange(event.target.value)
  }

  return (
    <textarea
      value={value}
      onChange={handleChange}
      placeholder={placeholder}
      name={name}
      className={cn(
        'border-[0.1rem] border-transparent outline-none bg-gray-2 w-full h-[4.2rem] rounded-[0.8rem] p-[2rem] min-h-[24rem]',
        className
      )}
    />
  )
}

export default Textarea
