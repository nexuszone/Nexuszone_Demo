import { cn } from '@/utils/cn'
import React, { FC, MouseEvent, ReactElement, useCallback } from 'react'

interface ButtonProps {
  title: string
  onClick?: (e: MouseEvent<HTMLButtonElement>) => void
  color?: string
  iconStart?: ReactElement | any
  iconEnd?: ReactElement | any
  before?: boolean
  variant?: 'icBdWhite' | 'blackLn'
  href?: string
  type?: 'submit' | 'reset' | 'button' | undefined
  setRef?: React.LegacyRef<HTMLButtonElement>
  disabled?: boolean
  className?: string
}

const Button: FC<ButtonProps> = ({
  type,
  title,
  variant,
  before,
  color,
  href,
  iconStart,
  iconEnd,
  onClick,
  setRef,
  disabled,
  className,
  ...rest
}) => {
  const IconStart = iconStart ? iconStart : undefined
  const IconEnd = iconEnd ? iconEnd : undefined
  const handleClickButton = useCallback(
    (e: React.MouseEvent<HTMLButtonElement>): void => {
      onClick && onClick(e)
    },
    [onClick]
  )

  if (variant === 'icBdWhite') {
    return (
      <button
        type={type}
        onClick={handleClickButton}
        ref={setRef}
        disabled={disabled}
        className={cn(
          'button group bg-white cursor-pointer text-black-1 flex items-center gap-[0.6rem] px-[1.6rem] pt-[0.6rem] pb-[5px] rounded-full overflow-hidden text-[1.4rem]  [transition:all_0.2s_ease-in-out] lg:hover:bg-[#e02a87]',
          className
        )}
        {...rest}
      >
        {IconStart && <IconStart className='w-[2rem]' />}
        <span>{title}</span>
        {IconEnd && <IconEnd />}
      </button>
    )
  } else if (variant === 'blackLn') {
    return (
      <button
        type={type}
        onClick={handleClickButton}
        ref={setRef}
        disabled={disabled}
        className={cn(
          'relative h-[6rem] flex items-center justify-center px-[2rem] rounded-[1rem] overflow-hidden before:absolute before:content-[""] before:w-full before:h-full before:left-[0] before:top-[0] before:bg-[linear-gradient(130deg,_rgba(67,74,84,1)_0%,_rgba(12,12,24,1)_100%)] before:[transition:all_0.3s_ease-in-out] hover:before:w-[300%] hover:before:opacity-75',
          className
        )}
        {...rest}
      >
        <span className='relative text-white z-10 text-[1.8rem] font-bold'>{title}</span>
      </button>
    )
  }

  return (
    <button
      type={type}
      onClick={handleClickButton}
      ref={setRef}
      disabled={disabled}
      className={cn(
        'text-white bg-purple-dark cursor-pointer  flex items-center rounded-[0.8rem] px-[1.6rem] pt-[1.2rem] pb-[1.1rem] text-[1.4rem] font-semibold [transition:all_0.2s_ease-in-out] lg:hover:bg-[#e02a87]',
        className
      )}
      {...rest}
    >
      {IconStart && <IconStart />}
      <span>{title}</span>
      {IconEnd && <IconEnd />}
    </button>
  )
}

export default React.memo(Button)
