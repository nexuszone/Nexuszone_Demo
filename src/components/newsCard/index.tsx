import { images } from '@/assets/data'
import { cn } from '@/utils/cn'
import { Link } from 'react-router-dom'
import { NewsCardTypes } from '../newsHome'

type NewsCardPropsTypes = {
  newsCard: NewsCardTypes
  className?: string
}

const NewsCard = ({ newsCard, className }: NewsCardPropsTypes) => {
  const { logo_main } = images

  return (
    <div
      className={cn(
        'NewsCard relative select-none bg-purple-dark rounded-[0.8rem] p-[1.6rem]',
        className
      )}
    >
      <div className='NewsCard-wrap flex flex-col h-full'>
        <Link
          to={newsCard?.link}
          className='NewsCard-img block relative pt-[calc(140/232*100%)] rounded-[0.8rem] bg-[rgb(12,_12,_24)] overflow-hidden mb-[2rem] group'
        >
          {newsCard?.image !== '' ? (
            <img
              src={newsCard?.image}
              className='w-full h-full z-2 object-cover absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 [transition:all_0.2s_ease-in-out] group-hover:scale-110'
              alt=''
            />
          ) : (
            <img
              src={logo_main}
              className='w-[12rem] h-[4rem] z-2 object-contain absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 [transition:all_0.2s_ease-in-out] group-hover:scale-x-110'
              alt=''
            />
          )}
        </Link>

        <div className='NewsCard-content text-gray-1 flex-1 flex flex-col'>
          <Link
            className='block text-white text-[1.4rem] font-bold mb-[2.4rem] line-clamp-2 min-h-[4.2rem] lg:hover:text-[#e02a87] [transition:all_0.2s_ease-in-out]'
            to={newsCard?.link}
          >
            {newsCard?.title}
          </Link>
          <p className='mb-[2.4rem] text-[1.3rem] line-clamp-3'>{newsCard?.description}</p>
          <span className='block mt-auto date italic text-[1.2rem] '>{newsCard?.date}</span>
        </div>
      </div>
    </div>
  )
}

export default NewsCard
