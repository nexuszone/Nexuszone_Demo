import { images } from '@/assets/data'
import { cn } from '@/utils/cn'
import { CardTypes } from '../aboutHome'

type CardPropsTypes = {
  card: CardTypes
  className?: string
}

const CardIntro = ({ className, card }: CardPropsTypes) => {
  const { logo_bg, logo_white } = images

  return (
    <div
      className={cn(
        'cardIntro relative select-none bg-purple-dark rounded-[0.8rem] p-[1.6rem] pb-[3.2rem] h-full',
        className
      )}
    >
      <div className='cardIntro-wrap'>
        <div className='cardIntro-img relative pt-[calc(150/304*100%)] rounded-[0.8rem] bg-[linear-gradient(148deg,_rgba(252,102,102,1)_0%,_rgba(233,4,134,1)_100%)] overflow-hidden mb-[2rem]'>
          <span className='absolute w-full h-full top-0 left-0 z-1'>
            <img src={logo_bg} className='object-contain m-auto' alt='' />
          </span>
          <img
            src={card?.icon}
            className='max-sm:w-[6rem] w-[8rem] h-[8rem] z-2 object-contain absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2'
            alt=''
          />
        </div>

        <span className='block w-fit mx-auto mb-[2rem] opacity-50'>
          <img src={logo_white} className='w-[5rem]' alt='' />
        </span>

        <div className='cardIntro-content text-white text-[1.6rem] text-center font-bold '>
          <p>{card?.title}</p>
        </div>
      </div>
    </div>
  )
}

export default CardIntro
