import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/effect-fade'
import 'swiper/css/pagination'
import CardIntro from '../cardIntro'
import { useTranslation } from 'react-i18next'
import { useState } from 'react'

export type CardTypes = {
  image?: string
  icon?: string
  title: string
}

const AboutHome = () => {
  const { t } = useTranslation()

  const cards = t('homeSectionTwo.card', { returnObjects: true })

  return (
    <div className='max-md:py-[0rem] max-md:pt-[2rem] py-[4rem]'>
      <div className='container'>
        <h2 className='max-sm:text-[2.5rem] max-md:mb-[1rem] text-white mb-[3.2rem] text-[3rem] font-bold'>
          {t('homeSectionOne.title')}
        </h2>
        <div className='max-md:mb-[2rem] content text-gray-1 mb-[8rem]'>
          <p>{t('homeSectionOne.desc1')}</p>
          <p>{t('homeSectionOne.desc2')}</p>
          <p>{t('homeSectionOne.desc3')}</p>
        </div>

        <Swiper
          spaceBetween={20}
          slidesPerView={4}
          speed={1000}
          pagination={{
            clickable: true
          }}
          breakpoints={{
            0: {
              slidesPerView: 1.2
            },
            375: {
              slidesPerView: 1.2
            },
            480: {
              slidesPerView: 1.8
            },
            720: {
              slidesPerView: 2.5
            },
            900: {
              slidesPerView: 3.5
            },
            1200: {
              slidesPerView: 4
            }
          }}
        >
          {JSON.parse(JSON.stringify(cards))?.map((card: CardTypes) => {
            return (
              <SwiperSlide key={card.title}>
                <CardIntro card={card} />
              </SwiperSlide>
            )
          })}
        </Swiper>
      </div>
    </div>
  )
}

export default AboutHome
