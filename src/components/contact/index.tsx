import Button from '../commom/buttons/Button'
import Input from '../commom/input/input'
import Textarea from '../commom/input/textarea'
import { useTranslation } from 'react-i18next'

const Contact = () => {
  const { t } = useTranslation()

  return (
    <div className='max-md:pt-[2rem] max-md:pb-[4rem] bg-white py-[11.6rem]'>
      <div className='container'>
        <div className='max-md:flex-col max-md:items-center max-md:justify-center flex mx-[-1.5rem]'>
          <div className='max-md:w-full w-1/2 flex-1 px-[1.5rem]'>
            <div className='max-w-[55.6rem]'>
              <h2 className='max-sm:text-[2.3rem] max-sm:mb-[1rem] font-bold text-[3rem] mb-[2rem]'>
                {t('contact.sectionLeft')}
              </h2>
              <p>{t('contact.sectionSubLeft')}</p>
            </div>
          </div>
          <div className='max-md:w-full w-1/2 flex-1 px-[1.5rem]'>
            <form>
              <div className='max-md:mt-[2rem] max-md:gap-y-[2rem] flex flex-wrap gap-y-[3rem] gap-x-[2rem]'>
                <div className='block flex-1 basis-full'>
                  <label className='block font-bold mb-[0.4rem]' htmlFor='name'>
                    {t('contact.sectionRight.name')} <span className='text-[#f54977]'>*</span>
                  </label>
                  <Input type='text' name='name' />
                </div>
                <div className='max-md:basis-full block flex-1 basis-1/3'>
                  <label className='block font-bold mb-[0.4rem]' htmlFor='email'>
                    {t('contact.sectionRight.email')} <span className='text-[#f54977]'>*</span>
                  </label>
                  <Input type='email' name='email' />
                </div>
                <div className='max-md:basis-full block flex-1 basis-1/3'>
                  <label className='block font-bold mb-[0.4rem]' htmlFor='phone'>
                    {t('contact.sectionRight.phone')}
                  </label>
                  <Input type='number' name='phone' />
                </div>
                <div className='max-md:basis-full block flex-1 basis-1/3'>
                  <label className='block font-bold mb-[0.4rem]' htmlFor='compannyName'>
                    {t('contact.sectionRight.company')}
                  </label>
                  <Input type='text' name='compannyName' />
                </div>
                <div className='block flex-1 basis-1/3'>
                  <label className='block font-bold mb-[0.4rem]' htmlFor='business'>
                    {t('contact.sectionRight.dept')}
                  </label>
                  <Input type='text' name='business' />
                </div>
                <div className='block flex-1 basis-full'>
                  <label className='block font-bold mb-[0.4rem]' htmlFor='message'>
                    {t('contact.sectionRight.message')} <span className='text-[#f54977]'>*</span>
                  </label>
                  <Textarea name='message' />
                </div>
                <Button title={t('button.submit')} variant='blackLn' className='w-full' />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Contact
