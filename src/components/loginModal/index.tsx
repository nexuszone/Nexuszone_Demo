import ModalV1 from '../modalV1'
import { Input } from '../ui/input'
import IconUserPlus from '@/icon/userPlus'
import { Button } from '../ui/button'
import { FC } from 'react'
import { Badge } from '../ui/badge'

interface LoginModalProps {
  handleClose: () => void
  show: boolean
}

const LoginModal: FC<LoginModalProps> = ({ handleClose, show }) => {
  return (
    <ModalV1 className='max-w-[500px]' handleClose={handleClose} show={show}>
      <div>
        <p className='text-[13px] text-[#8f909f] font-medium'>Please enter your ID or Join us!</p>
        <h4 className='text-[16px] mt-[.2em] mb-[0.425em] font-bold text-[#45485F] leading-1'>
          <span className='text-[#45485F] font-bold text-[16px] uppercase'>WAVE TOBE</span>{' '}
          Membership
        </h4>
        <hr className='mt-[15px] mb-[15px]' />
        <div className='pt-[15px] flex flex-col justify-center items-center'>
          <div className='flex items-center mb-10'>
            <img src='/img-home/aicon-clovercoin.png' className='w-[64px] h-[64px]' alt='' />
            <h4 className='uppercase font-bold text-[#45485F] leading-[100%] text-[30px] ml-1'>
              LOGIN
            </h4>
          </div>
          <div className='w-full flex flex-col gap-[20px]'>
            <div className='w-full relative'>
              <Input
                placeholder='아이디'
                className='p-2 text-[14px] w-full ring-0 border-[.5px] text-[#45485F]  outline-0 focus:outline-0 focus:ring-0 focus:ring-offset-0'
              />
              <IconUserPlus className='w-5 h-5 opacity-90 absolute right-[10px] top-2' />
            </div>
            <div className='w-full relative'>
              <Input
                type='password'
                placeholder='아이디'
                className='p-2 text-[14px] w-full ring-0 border-[.5px] text-[#45485F]  outline-0 focus:outline-0 focus:ring-0 focus:ring-offset-0'
              />
              <IconUserPlus className='w-5 h-5 opacity-90 absolute right-[10px] top-2' />
            </div>
            <Button className='text-center rounded-none h-[50px] leading-[1/5em] text-[17px]'>
              Login
            </Button>
          </div>
        </div>
        <div className='flex items-center justify-center mt-[60px] mx-auto'>
          <div className='flex items-center'>
            <Badge className='text-[#7A25FF] bg-[#F4EDFF] font-normal text-[12px]'>고객문의</Badge>
            <div className='ml-[10px] flex items-center'>
              <p className='font-normal text-[13px] text-[#45485F]'>궁금하신 사항이 있으신가요?</p>
              <img src='/img-home/line_ico.jpg' className='w-10 h-10' alt='' />
              <a href='' className='text-[13px] underline'>
                LINE_ID:@test
              </a>
            </div>
          </div>
        </div>
      </div>
    </ModalV1>
  )
}

export default LoginModal
