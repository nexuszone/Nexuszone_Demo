import { images } from '@/assets/data'
import { useTranslation } from 'react-i18next'
import { useMediaQuery } from 'usehooks-ts'

const BannerChild = () => {
  const { banner_ln, wm_dc } = images

  const matches = useMediaQuery('(max-width: 768px)')

  const { t } = useTranslation()

  return (
    <div className='bnchild relative overflow-hidden before:absolute before:content-[""] before:w-full before:h-[30%] before:bg-[linear-gradient(0deg,_rgba(12,12,24,1)_0%,_rgba(255,255,255,0)_100%)] before:bottom-[0] before:left-[0] before:z-50'>
      <div className='bnchild-bg absolute w-full h-full top-0 bottom-0'>
        <img src={banner_ln} className='w-full h-full object-fill object-bottom' alt='' />
      </div>
      <div className='container'>
        {matches ? (
          <div className='max-lg:min-h-[30rem] max-sm:min-h-[40rem] bnchild-wrap min-h-[58rem] relative z-40'>
            <h1 className='max-lg:absolute max-lg:bottom-[120px] max-lg:left-0 max-lg:right-0 max-md:absolute max-md:bottom-[120px] max-md:left-0 max-md:right-0 max-sm:bottom-[100px] max-sm:text-[3.5rem] text-center text-white text-[4rem] leading-[1.2] font-[700] translate-y-[2rem]'>
              {t('productSectionOneMB')}
            </h1>
            <div className='max-md:mx-[auto] max-md:my-[0] max-lg:mx-[auto] max-lg:my-[0] max-md:pt-[6rem] max-sm:pt-[6rem] max-sm:max-h-[40rem] mt-auto shrink-0 max-w-[58rem] max-h-[49.6rem]'>
              <img src={wm_dc} alt='' />
            </div>
          </div>
        ) : (
          <div className='bnchild-wrap min-h-[58rem] relative z-40 flex items-center justify-between gap-6 '>
            <h1 className='max-lg:text-[3.5rem] text-white text-[4rem] leading-[1.2] font-[700] translate-y-[2rem]'>
              {t('productSectionOne')}
            </h1>

            <div className='max-lg:w-[50rem] max-lg:py-10 mt-auto shrink-0 max-w-[58rem] max-h-[49.6rem]'>
              <img src={wm_dc} alt='' />
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

export default BannerChild
