import { images } from '@/assets/data'
import { Swiper, SwiperSlide } from 'swiper/react'
import { Autoplay, Pagination, EffectFade } from 'swiper/modules'
import 'swiper/css'
import 'swiper/css/effect-fade'
import 'swiper/css/pagination'
import { useTranslation } from 'react-i18next'

type SlidesTypes = {
  image: string
  title: string
}

const Banner = () => {
  const { logo_white } = images

  const { t } = useTranslation()

  const slides = t('banner.slides', { returnObjects: true })

  return (
    <div className='max-md:py-[0rem] max-md:pt-[2rem] bn relative w-full pt-[105px] pb-[3.6rem]'>
      <div className='bn-bg absolute top-0 w-full overflow-hidden pointer-events-none flex flex-col items-center justify-center'>
        <img
          src='/img-home/bg-home.png'
          className='object-fill pointer-events-none h-full max-h-[750px] w-[100vw]'
          alt=''
        />
      </div>
      <div className='container'>
        <div className='bn-wrap relative z-10'>
          <div className='max-sm:w-[80px] max-md:mt-[40px] max-md:mb-[20px] bn-logo w-[100px] mx-auto mb-[36px]'>
            <img src={logo_white} alt='' />
          </div>
          <div className='bnsl pagi-skew overflow-hidden'>
            <Swiper
              spaceBetween={0}
              slidesPerView={1}
              speed={1000}
              pagination={{
                clickable: true
              }}
              parallax
              effect={'fade'}
              grabCursor={true}
              centeredSlides={true}
              autoplay={{ delay: 5500 }}
              modules={[EffectFade, Pagination, Autoplay]}
            >
              {JSON.parse(JSON.stringify(slides)).map((slide: SlidesTypes, index: number) => {
                return (
                  <SwiperSlide key={index}>
                    <div className='max-sm:rounded-[12px] bnsl-item overflow-hidden rounded-[20px]'>
                      <div className='max-md:pt-[calc(450/670*100%)] bnsl-img relative pt-[calc(360/1400*100%)] overflow-hidden before:absolute before:content-[""] before:w-full before:h-full before:top-[0] before:left-[0] before:bg-[rgb(0,_0,_0,_29%)] before:backdrop-filter backdrop-blur-[1px] before:z-[11]'>
                        <img
                          src={slide.image}
                          className='absolute object-cover w-full h-full top-2/4 left-2/4 -translate-x-1/2 -translate-y-1/2 z-10'
                          alt=''
                          style={{ objectPosition: '50% 65%' }}
                        />
                      </div>
                      <div className='bnsl-content absolute w-full h-full top-0 left-0 flex items-center justify-center z-20'>
                        <p className='max-sm:text-[3rem] text-[40px] text-white font-bold'>
                          {slide.title}
                        </p>
                      </div>
                    </div>
                  </SwiperSlide>
                )
              })}
            </Swiper>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Banner
