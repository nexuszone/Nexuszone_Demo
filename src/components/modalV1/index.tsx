import IconX from '@/icon/x'
import { cn } from '@/utils/cn'
import { FC, ReactNode } from 'react'

interface ModelProps {
  className?: string
  children: ReactNode
  handleClose: () => void
  show: boolean
}

const ModalV1: FC<ModelProps> = ({ className, children, show, handleClose }) => {
  return (
    <div className={cn(' z-[51] fixed w-full  inset-0 ', !show && 'hidden')}>
      <div onClick={handleClose} className='absolute inset-0 bg-black/10'></div>

      <div className='flex justify-center items-center w-full h-full'>
        <div
          className={cn(
            'max-w-[714px] z-[52] bg-white py-6 px-[2.25em] w-full relative rounded-md',
            className
          )}
        >
          <button
            onClick={handleClose}
            className='w-10 h-10 text-center absolute
             bg-black text-white flex items-center justify-center right-[-15px]
              top-[-15px] rounded-full'
          >
            <IconX className='w-5 h-5 font-bold' />
          </button>
          {children}
        </div>
      </div>
    </div>
  )
}

export default ModalV1
