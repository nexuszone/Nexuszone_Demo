import { dataNews } from '@/assets/data'
import { Pagination } from 'antd'
import { useState } from 'react'
import NewsDetails from './newsDetail'
import Paragraph from 'antd/es/typography/Paragraph'
import { useTranslation } from 'react-i18next'

const News = () => {
  const [eleBlock, setBlock] = useState<boolean>(false)
  const numberOfDuplicates = 16
  const duplicatedArray: any = []
  let num = 1
  dataNews.forEach((item) => {
    for (let i = 0; i < numberOfDuplicates; i++) {
      num++
      duplicatedArray.push({ num, ...item })
    }
  })

  //// pagiantion
  const [currentPage, setCurrentPage] = useState(1)
  const pageSize = 10

  const startIndex = (currentPage - 1) * pageSize
  const endIndex = currentPage * pageSize

  const currentPageData = duplicatedArray.reverse().slice(startIndex, endIndex)

  const handlePageChange = (page: number) => {
    setCurrentPage(page)
  }

  const onShowSizeChange = (current: number, pageSize: number) => {
    setCurrentPage(1)
  }

  const handleDetails = (title: string) => {
    setBlock(true)
  }

  const { t } = useTranslation()

  return (
    <div className='max-md:pt-[3rem] max-md:pb-[4rem] py-[10.2rem] bg-white'>
      <div className='container'>
        {!eleBlock ? (
          <div className=''>
            <h2 className='max-md:mb-[1rem] mb-[2.4rem] text-[3rem] font-bold'>
              {t('news.oneTitleOnTable')}
            </h2>
            <div className='w-full mb-[2.4rem]'>
              <div className='w-full'>
                <div className='bg-black-2 rounded-[1.2rem] text-[1.4rem] font-bold text-white flex text-center'>
                  <div className='w-[9rem] pt-[2rem] pb-[1.9rem] px-[1.2rem]'>
                    {t('news.oneTable.col1')}
                  </div>
                  <div className='flex-1 pt-[2rem] pb-[1.9rem] px-[1.2rem]'>
                    {t('news.oneTable.col2')}
                  </div>
                  <div className='w-[15rem] pt-[2rem] pb-[1.9rem] px-[1.2rem]'>
                    {t('news.oneTable.col3')}
                  </div>
                </div>
              </div>
              <div>
                {currentPageData.map((item: any, index: number) => (
                  <div
                    key={index}
                    className='flex text-center text-[1.4rem] border-b-[0.1rem] border-black-1'
                  >
                    <div className='w-[8rem] pt-[2rem] pb-[1.9rem] px-[1.2rem]'>{item?.num}</div>
                    <div
                      className='flex-1 pt-[2rem] pb-[1.9rem] px-[1.2rem] [transition:all_0.2s_ease-in-out] text-left cursor-pointer hover:text-[#e02a87] font-bold'
                      onClick={() => handleDetails(item?.title)}
                    >
                      <Paragraph
                        ellipsis={{
                          rows: 2,
                          expandable: true
                        }}
                      >
                        {item?.title}
                      </Paragraph>
                    </div>
                    <div className='w-[15rem] pt-[2rem] pb-[1.9rem] px-[1.2rem]'>{item?.date}</div>
                  </div>
                ))}
              </div>
            </div>
            <Pagination
              showSizeChanger
              pageSize={pageSize}
              onShowSizeChange={onShowSizeChange}
              defaultCurrent={1}
              onChange={handlePageChange}
              total={duplicatedArray.length}
              className='w-fit mx-auto'
            />
          </div>
        ) : (
          <NewsDetails />
        )}
      </div>
    </div>
  )
}

export default News
