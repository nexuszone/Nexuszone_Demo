import { images } from '@/assets/data'
import { useTranslation } from 'react-i18next'

const NewsDetails = () => {
  const { chevronLeft, next_btn } = images

  const { t } = useTranslation()

  return (
    <div className=' bg-white'>
      <h2 className='max-md:mb-[1rem] mb-[1.6rem] text-[3rem] font-bold'>
        Nexus Bolsters đối tác với nội dung sòng bạc trực tiếp
      </h2>
      <div className='max-md:mb-[2rem] text-[#0c0c18] mb-[4.8rem]'>
        <span className='inline-block'>3.1k Xem</span>
        <span className='inline-block mx-[1rem]'>|</span>
        <span className='inline-block'>23rd Th1 2024</span>
      </div>
      <div className='max-md:py-[2rem] border-t-[0.1rem] border-b-[0.1rem] text-[#5c5c7e] border-gray-300 py-[3rem] min-h-[50rem] mb-[2.4rem]'>
        <p className='max-md:mb-[1.2rem] mb-[3.2rem]'>
          Xây dựng đội ngũ mạnh nhất của bạn với nhiều anh hùng khác nhau trong đa vũ trụ Nexus bằng
          cách lắp ráp chiến lược và tuyên bố chiến thắng! Để biết chi tiết về sự kiện thưởng Nexus,
          vui lòng tham khảo thông tin được cung cấp dưới đây.
        </p>
        <strong>Thời gian diễn ra sự kiện</strong>
        <p>- 3rd Th2 2024 ~ 12th Th2 2024 (UTC)</p>
        <strong>Phát phần thưởng</strong>
        <p>- Phần thưởng Nexus: Trong vòng 12th Th2 2024 sau khi sự kiện kết thúc</p>
        <p>- Phần thưởng token : Được công bố</p>
      </div>

      <div className='flex gap-[2rem] items-center'>
        <span className='cursor-pointer flex items-center gap-[1.2rem] [transition:all_0.2s_ease-in-out] hover:scale-110'>
          <img src={chevronLeft} alt='' />
          {t('pagination.first')}
        </span>
        <div className='flex gap-[2rem] items-center'>
          <span className='cursor-pointer [transition:all_0.2s_ease-in-out] hover:scale-110 '>
            <img src={next_btn} className='rotate-180' alt='' />
          </span>
          <span className='cursor-pointer [transition:all_0.2s_ease-in-out] hover:scale-110 '>
            <img src={next_btn} alt='' />
          </span>
        </div>
      </div>
    </div>
  )
}

export default NewsDetails
