import NewsCard from '../newsCard'
import 'swiper/css'
import 'swiper/css/effect-fade'
import 'swiper/css/pagination'
import { Swiper, SwiperSlide } from 'swiper/react'
import Button from '../commom/buttons/Button'
import { useTranslation } from 'react-i18next'

export type NewsCardTypes = {
  date: string
  description: string
  image?: string
  title: string
  link: string
}

const NewsHome = () => {
  const { t } = useTranslation()

  const newsCards = t('homeSectionThree.card', { returnObjects: true })

  return (
    <div className='max-md:pt-[4rem] max-sm:pb-[4rem] pt-[4rem] pb-[10.2rem]'>
      <div className='container'>
        <div className='max-md:mb-[2rem] flex items-center justify-between gap-1 mb-[3.2rem]'>
          <h2 className='max-sm:text-[2.5rem] text-white text-[3rem] font-bold'>
            {t('homeSectionThree.title')}
          </h2>

          <Button title={t('homeSectionThree.textBtnTitle')} />
        </div>
        <Swiper
          spaceBetween={20}
          slidesPerView={5}
          speed={1000}
          pagination={{
            clickable: true
          }}
          breakpoints={{
            0: {
              slidesPerView: 1.2
            },
            375: {
              slidesPerView: 1.5
            },
            480: {
              slidesPerView: 1.8
            },
            720: {
              slidesPerView: 2.5
            },
            900: {
              slidesPerView: 3.5
            },
            1200: {
              slidesPerView: 5
            }
          }}
        >
          {JSON.parse(JSON.stringify(newsCards)).map((newsCard: NewsCardTypes, index: number) => {
            return (
              <SwiperSlide key={index}>
                <NewsCard newsCard={newsCard} className='h-full' />
              </SwiperSlide>
            )
          })}
        </Swiper>
      </div>
    </div>
  )
}

export default NewsHome
