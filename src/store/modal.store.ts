import { create } from 'zustand'

export const useModelStore = create<UseModelStore>((set) => ({
  isShow: true,
  show: () => set((_) => ({ isShow: true })),
  close: () => set((_) => ({ isShow: false }))
}))

interface UseModelStore {
  isShow: boolean
  show: () => void
  close: () => void
}
