import BannerChild from '@/components/banner/BannerChild'
import FeatureMain from '@/components/featureMain'
import FooterChild from '@/components/footer/footerChild'
import InforCasino from '@/components/inforCasino/inforCasino'
import InforSolution from '@/components/inforCasino/inforSolution'

const ProductPage = () => {
  return (
    <main className='w-full max-w-full'>
      <BannerChild />
      <InforCasino />
      <FeatureMain />
      <InforSolution />
      <FooterChild />
    </main>
  )
}

export default ProductPage
