import BannerChildFour from '@/components/banner/BannerChildFour'
import Contact from '@/components/contact'
import FooterChild from '@/components/footer/footerChild'

const ContactPage = () => {
  return (
    <main className='w-full max-w-full'>
      <BannerChildFour />
      <Contact />
      <FooterChild />
    </main>
  )
}

export default ContactPage
