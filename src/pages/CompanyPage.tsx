import BannerChildSecond from '@/components/banner/BannerChildSe'
import FooterChild from '@/components/footer/footerChild'
import InforMission from '@/components/inforCasino/inforMission'
import Reputation from '@/components/reputation'
import RightSection from '@/components/rightsSection'

const CompanyPage = () => {
  return (
    <main className='w-full max-w-full'>
      <BannerChildSecond />
      <InforMission />
      <Reputation />
      <RightSection />
      <FooterChild />
    </main>
  )
}

export default CompanyPage
