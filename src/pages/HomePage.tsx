import AboutHome from '@/components/aboutHome'
import Banner from '@/components/banner'
import NewsHome from '@/components/newsHome'

const HomePage = () => {
  return (
    <main className='w-full max-w-full'>
      <Banner />
      {/* <div className='bg-black p-7'>
        <Button variant='icBdWhite' title={'Contact'} iconStart={sendMail}/>
        <Button title={'Đọc thêm'}/>
      </div> */}
      {/* <Button variant='yellow' title={'Test'} /> */}
      <AboutHome />
      <NewsHome />
    </main>
  )
}

export default HomePage
