import BannerChildThi from '@/components/banner/BannerChildThi'
import FooterChild from '@/components/footer/footerChild'
import News from '@/components/news'

const NewsPage = () => {
  return (
    <main className='w-full max-w-full'>
      <BannerChildThi />
      <News />
      <FooterChild />
    </main>
  )
}

export default NewsPage
