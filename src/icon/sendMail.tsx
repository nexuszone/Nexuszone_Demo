import { cn } from '@/utils/cn'

const sendMail = ({ className }: { className: string }) => {
  return <img src='/images/OutgoingMail.png' className={cn(className)} alt='' />
}

export default sendMail
